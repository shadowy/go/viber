# CHANGELOG

<!--- next entry here -->

## 0.3.0
2021-05-21

### Features

- extend action type (share-phone and location-picker) (05dd1d148cd90309ee29cf1300c14c89b0762a93)

## 0.2.0
2021-05-21

### Features

- extend parsed messages (contact, location, file) (41ae89b0728c531d37595b8bcafedfd96cb56af0)

## 0.1.0
2021-05-21

### Features

- prepare ci/cd (d0204b50648635625dc31316843c703f9044d302)

### Fixes

- comment lint (e0ec82c4cf50d0741a0362e1497fc57a259994f3)

