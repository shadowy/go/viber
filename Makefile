all: test lint

test:
	@echo ">> test"

lint: lint-golang-ci lint-card

lint-card:
	#@echo ">> lint-card"
	#@goreportcard-cli -v

lint-golang-ci:
	#@echo ">> lint-golang-ci"
	#@golangci-lint run
